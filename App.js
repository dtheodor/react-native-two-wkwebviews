import React, { Component } from 'react';
import { Button, Image, Switch, Text, TextInput, View, WebView } from 'react-native';
import WKWebView from 'react-native-wkwebview-reborn';

class Browser extends Component {
  constructor(props) {
    super(props);

    bloomingdales = 'https://www.bloomingdales.com'
    saks = 'https://www.saksfifthavenue.com'

    startingUrl = saks
    this.state = {
      // showWebView: true,
      urlInput: startingUrl,
      url: startingUrl,
      navState: 'initial',
      canGoBack_: false,
      canGoForward_: false
    }
  }
  _onNavigationStateChange(webViewState){
    console.log(webViewState)
    this.state.urlInput = webViewState.url
  }

  _onUrlChange(url){
    console.log("url change", url)
    // this.refs.webview.getBackForwardList().then((data) => {
    //   console.log("getBackForwardList", data)
    // })
    // this.refs.webview.canGoBack().then((data) => {
    //   console.log("canGoBack", data)
    // })
    // this.refs.webview.canGoForward().then((data) => {
    //   console.log("canGoForward", data)
    // })
    // this.state.urlInput = url
    this.setState({urlInput: url})
  }

  _onHistoryCurrentItemChange(data) {
    console.log("history change", data)
    if (data.type == "changeStarted") {
      console.log("history change start", data)
    } else if (data.type == "changeDone") {
      console.log("history change done", data)
    }
    this.setState({
      canGoBack_: data.canGoBack,
      canGoForward_: data.canGoForward
    })
  }

  _onProgress(progress){
    // console.log(progress)
  }

  // componentWillUnmount(){
  //   console.warn('unmounting!!!', this)
  // }
  render() {
    return (
      <View style={{flex: 1, padding: 5}}>
        <TextInput
          placeholder="URL"
          defaultValue={this.state.urlInput}
          onChangeText={(text) => this.setState({urlInput: text})}
        />
        <View style={{flex: 0.08, flexDirection: 'row', height: 50}}>
          {this.state.canGoBack_ ?
          <Button
            onPress={() => this.refs.webview.goBack()}
            title="Back"
          /> : 
          <Button
            onPress={() => void(0)}
            title="Back"
            disabled={true}
            />
          }
          {this.state.canGoForward_ ?
          <Button
            onPress={() => this.refs.webview.goForward()}
            title="Forward"
          /> :
          <Button
            onPress={() => void(0)}
            title="Forward"
            disabled={true}
          />
          }
          <Button
            onPress={() => this.setState({url: this.state.urlInput})}
            title="Go"
          />
        </View>
        <Text>
          {this.state.navState}
        </Text>
        <WKWebView
          ref={"webview"}
          source={{uri: this.state.url}}
          sendCookies={true}
          onLoadStart={() => this.setState({navState: 'loading'})}
          onLoadEnd={() => this.setState({navState: 'loaded'})}
          //onNavigationStateChange={this._onNavigationStateChange.bind(this)}
          onHistoryCurrentItemChange={this._onHistoryCurrentItemChange.bind(this)}
          onUrlChange={this._onUrlChange.bind(this)}
          onProgress={this._onProgress.bind(this)}
        />
      </View>
    );
  }
}


class Shit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // showWebView: true,
      urlInput: 'https://www.bloomingdales.com',
      url: 'https://www.google.com',
      navState: 'initial'}
  }

  componentWillUnmount(){
    console.warn('unmounting!!!', this)
  }
  render() {
    return (
      <View style={{flex: 1, padding: 5}}>
        <TextInput
          placeholder="URL"
          defaultValue={this.state.urlInput}
          onChangeText={(text) => this.setState({urlInput: text})}
        />
        <Button
          onPress={() => this.setState({url: this.state.urlInput})}
          title="Go"
        />
        <Text>
          {this.state.navState}
        </Text>
        <WKWebView
          source={{uri: this.state.url}}
          onLoadStart={() => this.setState({navState: 'loading'})}
          onLoadEnd={() => this.setState({navState: 'loaded'})}
          onNavigationStateChange={this._onNavigationStateChange.bind(this)}
        />
      </View>
    );
  }
}

class OneBrowser extends Component{
  render() {
    return (
      <View style={{flex: 1, padding: 20}}>
        <Browser />
      </View>
    );
  }
}



class ToggleableBrowser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showBrowser: false
    };
    this.browser = <Browser />
    this.text = (
      <View style={{flex: 1, padding: 5}}>
        <Text>laalala</Text>
        <WKWebView source={{uri: 'https://www.asdf.com'}}/>
      </View>
      );
  }
  _switchWebView(){
    this.setState({showBrowser: !this.state.showBrowser})
  }
  render(){
    var browser = this.state.showBrowser ? this.browser : null;
    var txt = this.state.showBrowser ? this.text : null;

    return (
      <View style={{flex: 1, padding: 5}}>
        <Button
          onPress={() => this.setState({showBrowser: !this.state.showBrowser})}
          title={this.state.showBrowser ? "Hide" : "Show"}
        />
        {this.state.showBrowser && browser}
      </View>
    );
  }
}

class TwoToggleableBrowsers extends Component {
  constructor(props){
    super(props);
    this.state = {
      showBrowser1: true,
      showBrowser2: true
    };
  }
  render() {
    let pic = {
      uri: 'https://upload.wikimedia.org/wikipedia/commons/d/de/Bananavarieties.jpg'
    };

    var browser1 = this.state.showBrowser1 ? <ToggleableBrowser /> : null;
    var browser2 = this.state.showBrowser2 ? <ToggleableBrowser /> : null;

    return (
      <View style={{flex: 1, padding: 20}}>
        {/* <Switch 
          onValueChage={(value) => {this.setState({showBrowser1: value})}}
          value={this.state.showBrowser1}
        />
        <Switch 
          onValueChage={(value) => {this.setState({showBrowser2: value})}}
          value={this.state.showBrowser2}
        /> */}
        <View style={{flex: 1}}>
          {browser1}
          {browser2}
        </View>
      </View>


    // <View style={{flex: 1}}>
    //   <Browser />
    //   {/* <Browser />
    //   <View style={{flex: 1, backgroundColor: 'powderblue'}}>
    //     <WKWebView source={{uri: 'https://www.bloomingdales.com'}} />
    //   </View>
    //   <View style={{flex: 1, backgroundColor: 'steelblue'}}>
    //     <WKWebView source={{uri: 'https://www.bloomingdales.com'}} />
    //   </View> */}
    // </View>
    );
  }
}

export default OneBrowser;